#!/bin/bash

# Credit: https://github.com/nathany/hugo-deploy/blob/master/ci-install-s3deploy.sh
S3DEPLOY_DOWNLOAD=s3deploy_${S3DEPLOY_VERSION}_Linux-64bit.tar.gz

set -x
set -e

# Install s3deploy if not already cached or upgrade an old version.
if [ ! -e $HOME/bin/s3deploy ] || ! [[ `$HOME/bin/s3deploy -V` =~ ${S3DEPLOY_VERSION} ]]; then
  wget https://github.com/bep/s3deploy/releases/download/v${S3DEPLOY_VERSION}/${S3DEPLOY_DOWNLOAD}
  tar xvzf ${S3DEPLOY_DOWNLOAD} s3deploy
  mv s3deploy $HOME/bin/s3deploy
fi
